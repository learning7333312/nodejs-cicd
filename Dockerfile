# This file is a template, and might need editing before it works on your project.
FROM node:8.11

WORKDIR /app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /app/
RUN npm install

COPY . /app

# replace this with your application's default port
EXPOSE 8080
CMD [ "npm", "start" ]